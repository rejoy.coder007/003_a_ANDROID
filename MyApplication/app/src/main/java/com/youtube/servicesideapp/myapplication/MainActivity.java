package com.youtube.servicesideapp.myapplication;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity   {

    int count=0;
    Boolean mStopLoop=false;



    Handler handler;
    private TextView displayTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("THREAD", " main id: " + Thread.currentThread().getId());



        //Who execute tasks ..? looper thread
       handler = new Handler(getApplicationContext().getMainLooper());
        displayTextView = (TextView)findViewById(R.id.countText);
    }

    public void StartThread(View v)
    {
        Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();


        new Thread(new Runnable() {
            public void run(){
                mStopLoop = true;

                while(mStopLoop)
                {

                    try {
                        Thread.sleep(1000);

                        displayTextView.post(new Runnable() {
                            @Override
                            public void run() {


                                displayTextView.setText(":"+count);
                            }
                        });

                        count++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.i("THREAD", " thread id: " + Thread.currentThread().getId());
                }
            }
        }).start();



    }

    public void StopThread(View v)
    {
        mStopLoop=false;
        Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();

    }




}
